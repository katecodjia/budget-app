import 'package:flutter/material.dart';

import '../../../export.dart';

final $appColors = AppColors();

class AppColors {
  final Color primary = const Color(0xFF5d5fef);
  final Color text = const Color(0xFF090909);
  final Color lightBlack = const Color(0xFFacb0b4);
  final Color lightGray = const Color(0xFFf1f1f1);
  final Color white = const Color(0xFFFFFFFF);
  final Color black = const Color(0xFF000000);
  final Color success = const Color(0xFF2bba75);
  final Color danger = const Color(0xFFe95c5d);
  final Color gray = const Color(0xFFf6f6f6);
  final Color orange = const Color(0xFFf56374);
  final Color violet = const Color(0xFFc077e3);
  final bool isDark = false;

  Color shift(Color c, double d) => ColorUtils.shiftHsl(c, d * (isDark ? -1 : 1));

  ThemeData toThemeData() {
    /// Create a TextTheme and ColorScheme, that we can use to generate ThemeData
    TextTheme txtTheme = (isDark ? ThemeData.dark() : ThemeData.light()).textTheme;
    Color txtColor = black;
    ColorScheme colorScheme = ColorScheme(
      brightness: isDark ? Brightness.dark : Brightness.light,
      primary: primary,
      surfaceTint: white,
      primaryContainer: black,
      secondary: black,
      secondaryContainer: gray,
      background: white,
      surface: white,
      onBackground: txtColor,
      onSurface: txtColor,
      onError: danger,
      onPrimary: Colors.white,
      onSecondary: Colors.white,
      error: danger,
    );

    /// Now that we have ColorScheme and TextTheme, we can create the ThemeData
    /// Also add on some extra properties that ColorScheme seems to miss
    var t = ThemeData.from(
      useMaterial3: true,
      textTheme: txtTheme,
      colorScheme: colorScheme,

      //colorSchemeSeed:
    ).copyWith(
        textSelectionTheme: TextSelectionThemeData(cursorColor: primary),
        highlightColor:  Colors.transparent,
        splashColor: Colors.transparent,
        appBarTheme:  AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle.light, // 2
          color: $appColors.white,
        ),
        textTheme: TextTheme(
          bodyLarge: AppTypography().body1,
        ),
        tabBarTheme: TabBarTheme(
          labelColor: white,
          indicator: BoxDecoration(
            color: primary,
            borderRadius: BorderRadius.circular(100),
          ),
          dividerColor: Colors.transparent,
        ),
        inputDecorationTheme: InputDecorationTheme(
          errorMaxLines: 2,
          hintStyle: AppTypography()
              .body2.copyWith(color: $appColors.lightBlack),
          border: InputBorder.none,
          errorStyle: AppTypography().text2.copyWith(color: $appColors.danger),
          contentPadding: const EdgeInsets.fromLTRB(16, 5, 10, 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                color: lightBlack.withOpacity(0.3)
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color:danger)
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: primary)),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: danger)),
        )
    );

    /// Return the themeData which MaterialApp can now use
    return t;
  }

}

class ColorUtils {
  static Color shiftHsl(Color c, [double amt = 0]) {
    var hslc = HSLColor.fromColor(c);
    return hslc.withLightness((hslc.lightness + amt).clamp(0.0, 1.0)).toColor();
  }

  static Color parseHex(String value) => Color(int.parse(value.substring(1, 7), radix: 16) + 0xFF000000);

  static Color blend(Color dst, Color src, double opacity) {
    return Color.fromARGB(
      255,
      (dst.red.toDouble() * (1.0 - opacity) + src.red.toDouble() * opacity).toInt(),
      (dst.green.toDouble() * (1.0 - opacity) + src.green.toDouble() * opacity).toInt(),
      (dst.blue.toDouble() * (1.0 - opacity) + src.blue.toDouble() * opacity).toInt(),
    );
  }
}