import 'package:flutter/material.dart';
import 'package:budget_app/export.dart';
import 'package:intl/intl.dart';

import '../../../../../../common_widget/snack_bar.dart';
import '../../../../../../config/router/route_utils.dart';
import '../../../../data/data_sources/local/database_helper.dart';
import '../../../../domain/entities/transactions.dart';
class TransactionProvider with ChangeNotifier {
   List<Transactions> _transactions = [];
   final DatabaseHelper _dbHelper = DatabaseHelper();

  List<Transactions> get transactions => _transactions;
  final date = TextEditingController();
  final price = TextEditingController();
  final name = TextEditingController();

   void getDate(String value)  {
     date.text=value;
     notifyListeners();
   }
   void getPrice(String value)  {
     price.text=value;
     notifyListeners();
   }
   void getName(String value)  {
     name.text=value;
     notifyListeners();
   }

  bool get validate {
    return name.text.isEmpty|| date.text.isEmpty||price.text.isEmpty;
  }

   TransactionProvider() {
     _fetchTransactions();
   }

   Future<void> _fetchTransactions() async {
     _transactions = await _dbHelper.getTransactions();
     notifyListeners();
   }

   Future<void> addTransaction(BuildContext context) async {
     await _dbHelper.insertTransaction(Transactions(name: name.text.capitalize(), date: date.text, price: price.text));
     await _fetchTransactions();
     showSnackBar(context: context, message: 'Transactions add successfully');
     context.push(AppPage.dashboard.toPath);
     date.clear();
     price.clear();
     name.clear();
   }

   Future<void> deleteTransaction(int? id,BuildContext context) async {
     await _dbHelper.deleteTransaction(id);
     await _fetchTransactions();
     showSnackBar(context: context, message: 'Transactions delete successfully');
     Navigator.pop(context);
   }


   double get totalBalance {
     return _transactions.fold(0, (sum, item) => sum + double.parse(item.price) * (item.isIncome ? 1 : -1));
   }

   double get totalExpenses {
     return _transactions
         .where((transaction) => !transaction.isIncome)
         .fold(0, (sum, item) => sum + double.parse(item.price));
   }

   double get totalIncome {
     return _transactions
         .where((transaction) => transaction.isIncome)
         .fold(0, (sum, item) => sum + double.parse(item.price));
   }

   String get formattedTotalBalance {
     final formatter = NumberFormat.currency(symbol: '\$');
     return formatter.format(totalBalance);
   }

   String get formattedTotalExpenses {
     final formatter = NumberFormat('#,##0.000');
     return formatter.format(totalExpenses);
   }

   String get formattedTotalIncome {
     final formatter = NumberFormat('#,##0.000');
     return formatter.format(totalIncome);
   }

}
