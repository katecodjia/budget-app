import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import '../router/app_router.dart';
import 'color/app_color.dart';

class AppTheme extends StatelessWidget {
  const AppTheme({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
        routeInformationProvider: AppRouter.router.routeInformationProvider,
        routeInformationParser: AppRouter.router.routeInformationParser,
        routerDelegate: AppRouter.router.routerDelegate,
        theme: AppColors().toThemeData(),
        title: "BudgetApp",
        debugShowCheckedModeBanner: false,
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('en')
        ],
        locale: const Locale('en', 'EN'),
      );

  }
}
