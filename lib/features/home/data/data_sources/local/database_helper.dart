import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../../../domain/entities/transactions.dart';
class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper._internal();
  static Database? _database;

  factory DatabaseHelper() {
    return _instance;
  }

  DatabaseHelper._internal();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    String path = join(await getDatabasesPath(), 'transactions.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute(
        '''
      CREATE TABLE transactions(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name TEXT,
          date TEXT,
          price TEXT,
          isIncome INTEGER
      )
      '''
    );
    await _insertInitialData(db);
  }

  Future<void> _insertInitialData(Database db) async {
    final initialTransactions = [
      Transactions(name: 'Salary', date: 'Avril 04 2023', price: '2300', isIncome: true),
      Transactions(name: 'Groceries', date: 'Avril 04 2023', price: '230'),
      Transactions(name: 'Phone Bill', date: 'Avril 04 2023', price: '30'),
      Transactions(name: 'Rent', date: 'Avril 04 2023', price: '30'),
    ];

    for (var transaction in initialTransactions) {
      await db.insert('transactions', transaction.toMap());
    }
  }

  Future<List<Transactions>> getTransactions() async {
    Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('transactions');

    return List.generate(maps.length, (i) {
      return Transactions(
        id: maps[i]['id'],
        name: maps[i]['name'],
        date: maps[i]['date'],
        price: maps[i]['price'],
        isIncome: maps[i]['isIncome'] == 1,
      );
    });
  }

  Future<void> insertTransaction(Transactions transaction) async {
    Database db = await database;
    await db.insert(
      'transactions',
      transaction.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteTransaction(int? id) async {
    Database db = await database;
    await db.delete(
      'transactions',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}

