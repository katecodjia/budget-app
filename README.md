# Budget App

Budget App is a mobile application designed to manage daily expenses, finances and transactions.

Packages used :
-  flutter_screenutil
- go_router
-  intl
- flutter_phoenix
-  provider
- flutter_localization
- dio
- sqflite
