import 'package:flutter/material.dart';
import '../../../export.dart';
import '../app_inkwell.dart';


class AppButton extends StatelessWidget {
  const AppButton(
      {super.key,
      this.backgroundColor,
       this.text,
      required this.onTap,
      this.textColor = Colors.white,
      this.height,
      this.textStyle,
      this.width,
      this.raduis,
      this.borderColor,
      this.widget,
      this.borderWith, this.gradient, this.boxShadow, this.padding});
  final String? text;
  final VoidCallback? onTap;
  final Color? backgroundColor;
  final Color? textColor;
  final Color? borderColor;
  final double? height;
  final double? width;
  final double? borderWith;
  final BorderRadiusGeometry? raduis;
  final TextStyle? textStyle;
  final Widget? widget;
 final Gradient? gradient;
  final List<BoxShadow>? boxShadow;
  final EdgeInsetsGeometry? padding;
  @override
  Widget build(BuildContext context) {
    return AppInkwell(
      onTap: onTap,
      child: Container(
        width: width ?? double.infinity,
        height: height ?? (showSmallScreenView(context) ? 50.0 : 52.0),
        padding: padding,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: backgroundColor ?? $appColors.primary,
          borderRadius: raduis ?? BorderRadius.circular(10),
          border: Border.all(
            color: borderColor ?? Colors.transparent,
            width: 1.0,
          ),
          gradient: gradient,
          boxShadow: boxShadow,

        ),
        child: widget?? Text(
          text??"",
          style:
              textStyle ?? AppTypography().text1.copyWith(color:  textColor??$appColors.white),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
