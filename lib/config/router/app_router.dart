import 'package:budget_app/config/router/route_utils.dart';
import 'package:flutter/material.dart';
import 'package:budget_app/export.dart';
class AppRouter {
  static final _rootNavigatorKey = GlobalKey<NavigatorState>();
  static final _shellNavigatorKey = GlobalKey<NavigatorState>();

  /// The route configuration.
  static final GoRouter _goRouter = GoRouter(
    navigatorKey: _rootNavigatorKey,
    debugLogDiagnostics: false,
    initialLocation: AppPage.dashboard.toPath,
    errorBuilder: (context, state) {
      return const Scaffold(
        body: Center(
          child: Text(
            " Oups route error :)",
            style: TextStyle(color: Colors.black),
          ),
        ),
      );
    },
      routes: [
        ShellRoute(
            navigatorKey: _shellNavigatorKey,
            builder: (context, state, child) {
              return child;
            },
            routes: [
              GoRoute(
                  path: AppPage.dashboard.toPath,
                  name: AppPage.dashboard.toName,
                  builder: (context, state) => CustomBottomNavigationBar(),
                  routes: [
                    GoRoute(
                      path: "home",
                      pageBuilder: (context, state) => CustomTransitionPage(
                        key: state.pageKey,
                        child: const HomeScreenView(),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          return FadeTransition(
                            opacity: CurveTween(curve: Curves.bounceIn)
                                .animate(animation),
                            child: child,
                          );
                        },
                      ),
                    ),
                  ]
              ),
            ]),

      ]

  );

  static GoRouter get router => _goRouter;
}
