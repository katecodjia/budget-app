import 'package:budget_app/export.dart';
import 'package:flutter/material.dart';
import '../../domain/entities/transactions.dart';
import '../provider/tansactions/local/transactions_provider.dart';

class TransactionCards extends StatelessWidget {
  const TransactionCards(
      {super.key,
      required this.transactions,
      required this.index});
  final Transactions transactions;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
        key: Key(transactions.id.toString()),
        background: Container(
          color: $appColors.danger,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          alignment: Alignment.centerLeft,
          child: Icon(
            Icons.delete,
            color: $appColors.white,
          ),
        ),
        confirmDismiss: (direction) async {
    if (direction == DismissDirection.startToEnd) {

    bool dismiss = false;
          await showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text(
                    "Are you sure you want to delete this transaction?",
                    style: AppTypography()
                        .heading2
                        .copyWith(color: $appColors.text),
                    textAlign: TextAlign.center,
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          dismiss = false;
                          Navigator.pop(context);
                        },
                        child: Text(
                          "No",
                          style: AppTypography()
                              .body1
                              .copyWith(color: $appColors.primary),
                        )),
                    Consumer<TransactionProvider>(builder:
                        (BuildContext context, transactionProvider,
                            Widget? child) {
                      return TextButton(
                          onPressed: () async {
                            dismiss = true;
                            // Remove the transaction from the local list immediately
                            transactionProvider.transactions.removeAt(index);
                            // Remove the transaction from database
                            await transactionProvider.deleteTransaction(transactions.id,context);
                          },
                          child: Text(
                            "Yes",
                            style: AppTypography()
                                .body1
                                .copyWith(color: $appColors.danger),
                          ));
                    }),
                  ],
                );
              });
          return dismiss;
        }
    return null;
        },
        child: GestureDetector(
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: $appColors.white,
                borderRadius: BorderRadius.circular(6),
                border: Border.all(color: $appColors.lightGray)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      transactions.name,
                      style: AppTypography()
                          .text1
                          .copyWith(color: $appColors.black),
                    ),
                    const SpaceH(5),
                    Text(transactions.date, style: AppTypography().text3),
                  ],
                ),
                transactions.isIncome
                    ? Text(
                        '\$ ${transactions.price}',
                        style: AppTypography()
                            .heading3
                            .copyWith(color: $appColors.success),
                      )
                    : Text(
                        '-\$ ${transactions.price}',
                        style: AppTypography()
                            .heading3
                            .copyWith(color: $appColors.black),
                      ),
              ],
            ),
          ),
        ));
  }
}
