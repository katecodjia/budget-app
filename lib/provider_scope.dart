import 'package:flutter/material.dart';
import 'export.dart';
import 'features/bottom_navigation/presentation/provider/bottom_navigator_provider.dart';
import 'features/home/presentation/provider/tansactions/local/transactions_provider.dart';

class ProviderScope extends StatelessWidget {
  const ProviderScope({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<BottomNavigatorProvider>(create: (_) => BottomNavigatorProvider()),
        ChangeNotifierProvider<TransactionProvider>(create: (_) => TransactionProvider()),
      ],
      child:AnnotatedRegion(
        value:  SystemUiOverlayStyle(
          statusBarColor: $appColors.white,
          statusBarIconBrightness: Brightness.dark,
        ),
        child:child,
      )
      
      
    );
  }
}
