import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SpaceW extends StatelessWidget {
  final num width;
  const SpaceW(this.width, {super.key});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width.w,
    );
  }
}

class SpaceH extends StatelessWidget {
  final num height;
  const SpaceH(this.height, {super.key});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height.h,
    );
  }
}

double phoneHeight(BuildContext context){
  return MediaQuery.sizeOf(context).height;
}

double phoneWidth(BuildContext context){
  return MediaQuery.sizeOf(context).width;
}

bool showSmallScreenView(BuildContext context){
  return phoneWidth(context) < 400;
}