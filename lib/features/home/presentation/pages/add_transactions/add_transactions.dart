import 'package:budget_app/export.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../../common_widget/button/app_button.dart';
import '../../provider/tansactions/local/transactions_provider.dart';
class AddTransaction extends StatefulWidget {
  const AddTransaction({super.key});

  @override
  State<AddTransaction> createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {


  @override
  Widget build(BuildContext context) {
    return  Consumer<TransactionProvider>(
        builder: (BuildContext context, transactionProvider, Widget? child) {
          return LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return  AnimatedContainer(
                  duration: const Duration(milliseconds: 300),
                  padding: const EdgeInsets.all(30).copyWith(  bottom: MediaQuery.of(context).viewInsets.bottom+30,
                  ),
                  child:  Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          children: [
                            GestureDetector(
                              onTap: (){
                                context.pop();
                              },
                              child: Container(
                                height:42,
                                width: 42,
                                decoration: BoxDecoration(
                                    color: $appColors.white,
                                    shape: BoxShape.circle,
                                    border:Border.all(color: $appColors.lightGray,)
                                ),
                                child:  Icon(Icons.arrow_back, color:$appColors.black),
                              ),
                            ),

                            const SpaceW(15),
                            Text('Add a new transaction',style: AppTypography().heading2.copyWith(color: $appColors.black),)
                          ],
                        ),
                        const SpaceH(15),
                        TextFormField(
                          readOnly: true,
                          controller: transactionProvider.date,
                          onChanged: transactionProvider.getDate,
                          decoration: InputDecoration(
                              hintText: '13 May 2023',
                              suffixIcon: IconButton(
                                onPressed: () {  },
                                icon: Icon(Icons.calendar_month_outlined,color: $appColors.black,),

                              )
                          ),
                          onTap: () async {
                            DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2101),
                            );
                            if (pickedDate != null) {
                              String formattedDate =
                              DateFormat(" d MMMM yyyy", "en").format(
                                  pickedDate);
                              transactionProvider.date.text = formattedDate;
                            } else {
                              print("Date is not selected");
                            }
                          },
                        ),
                        const SpaceH(15),
                        TextFormField(
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          controller:  transactionProvider.price,
                          onChanged: transactionProvider.getPrice,
                          decoration: InputDecoration(
                              hintText: '300',
                              suffixIcon: IconButton(
                                onPressed: () {  },
                                icon: Icon(Icons.monetization_on_outlined,color: $appColors.black,),

                              )
                          ),
                        ),
                        const SpaceH(15),
                        TextFormField(
                          controller:  transactionProvider.name,
                          onChanged: transactionProvider.getName,
                          decoration: InputDecoration(
                              hintText: 'Groceries...',
                              suffixIcon: IconButton(
                                onPressed: () {  },
                                icon: Icon(Icons.edit_sharp,color: $appColors.black,),

                              )
                          ),
                        ),
                        const SpaceH(30),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            AppButton(
                              onTap: (){
                                context.pop();
                                transactionProvider.date.clear();
                                transactionProvider.price.clear();
                                transactionProvider.name.clear();
                              },
                              text: 'Cancel',
                              width: phoneWidth(context)/2.5,
                              backgroundColor: $appColors.white,
                              textColor: $appColors.text.withOpacity(0.8),
                              boxShadow: [
                                BoxShadow(
                                    color: $appColors.black.withOpacity(0.25),
                                    spreadRadius: 0,
                                    blurRadius: 4,
                                    offset: const Offset(1, 1))
                              ],
                            ),
                            AppButton(
                              onTap:
                              transactionProvider.validate
                                  ?null
                                  : (){
                                transactionProvider.addTransaction(context);
                              },
                              backgroundColor:transactionProvider.validate?$appColors.lightBlack:$appColors.primary,
                              text: 'Add',
                              width: phoneWidth(context)/2.5,
                              boxShadow: [
                                BoxShadow(
                                    color: $appColors.black.withOpacity(0.25),
                                    spreadRadius: 0,
                                    blurRadius: 4,
                                    offset: const Offset(1, 1))
                              ],
                            ),
                          ],
                        )

                      ],
                    ),
                  )
              );
            },);});
    }


  }

