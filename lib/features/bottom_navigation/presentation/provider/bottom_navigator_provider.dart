import 'package:flutter/material.dart';

class BottomNavigatorProvider extends ChangeNotifier {
  int screenIndex = 0; // Initial index of the screen
  // function to return the current screen Index
  int get fetchCurrentScreenIndex => screenIndex;

  void updateScreenIndex(int newIndex) {
    // function to update the screenIndex
    screenIndex = newIndex;
    notifyListeners();
  }
}