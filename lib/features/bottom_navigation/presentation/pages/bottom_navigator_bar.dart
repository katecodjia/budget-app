import 'package:budget_app/features/bottom_navigation/presentation/provider/bottom_navigator_provider.dart';
import 'package:flutter/material.dart';
import 'package:budget_app/export.dart';

class CustomBottomNavigationBar extends StatelessWidget {
   CustomBottomNavigationBar({super.key});
 final  List<dynamic> screens = [
    const HomeScreenView(),
    Center(child: Text('Transactions',style: AppTypography().text1,),),
    Center(child: Text('Statistics',style: AppTypography().text1,),),
    Center(child: Text('Settings',style: AppTypography().text1,),),


  ];
  @override
  Widget build(BuildContext context) {

    return Consumer<BottomNavigatorProvider>(
        builder: (BuildContext context, provider, Widget? child){
          return Scaffold(
            bottomNavigationBar: BottomAppBar(
              height: 70,
              elevation: 5,
              padding: EdgeInsets.zero,
              child: BottomNavigationBar(
                backgroundColor: $appColors.white,
                type:BottomNavigationBarType.fixed,
                showSelectedLabels: true,
                elevation: 5,
                selectedItemColor: $appColors.primary,
                selectedLabelStyle: AppTypography().text1,
                unselectedLabelStyle: AppTypography().text1,
                unselectedItemColor: $appColors.text.withOpacity(0.8),
                currentIndex: provider.fetchCurrentScreenIndex,
                onTap: (value) => provider.updateScreenIndex(value),
                items: [
                  BottomNavigationBarItem(
                      label: 'Home',
                      icon: Icon(
                          (provider.fetchCurrentScreenIndex == 0) ? Icons.home : Icons.home_outlined),
                      backgroundColor: Colors
                          .indigo // provide color to any one icon as it will overwrite the whole bottombar's color ( if provided any )
                  ),
                  BottomNavigationBarItem(
                    label: 'Transactions',
                    icon: Icon((provider.fetchCurrentScreenIndex == 1)
                        ? Icons.swap_horiz
                        : Icons.swap_horiz_outlined),
                  ),
                  BottomNavigationBarItem(
                    label: 'Statistics',
                    icon: Icon((provider.fetchCurrentScreenIndex == 2)
                        ? Icons.bar_chart
                        : Icons.bar_chart_outlined),
                  ),
                  BottomNavigationBarItem(
                    label: 'Settings',
                    icon: Icon((provider.fetchCurrentScreenIndex == 3)
                        ? Icons.settings
                        : Icons.settings_outlined),
                  ),
                ],
              ),
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            floatingActionButton: Container(
              margin: const EdgeInsets.only(top: 10),
              height: 54,
              width: 54,
              child: FloatingActionButton(
                backgroundColor: $appColors.primary,
                elevation: 0,
                onPressed: (){
                  showModalBottomSheet<void>(
                    context: context,
                    isScrollControlled: true,
                    isDismissible: false,
                    builder: (BuildContext context) {
                      return const AddTransaction();
                    },
                  );
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                child:  Icon(
                  Icons.add,
                  color: $appColors.white,
                ),
              ),
            ),
            body: screens[provider.fetchCurrentScreenIndex],
          );
        }
    );

  }

}

// import 'package:budget_app/export.dart';
// import 'package:flutter/material.dart';
//
// import '../../domain/entities/nav_model.dart';
// import '../widgets/nav_bar.dart';
//
// class CustomBottomNavigationBar extends StatefulWidget {
//   const CustomBottomNavigationBar({super.key});
//
//   @override
//   State<CustomBottomNavigationBar> createState() => _CustomBottomNavigationBarState();
// }
//
// class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
//   final homeNavKey = GlobalKey<NavigatorState>();
//   final searchNavKey = GlobalKey<NavigatorState>();
//   final notificationNavKey = GlobalKey<NavigatorState>();
//   final profileNavKey = GlobalKey<NavigatorState>();
//   int selectedTab = 0;
//   List<NavModel> items = [];
//
//   @override
//   void initState() {
//     super.initState();
//     items = [
//       NavModel(
//         page: const HomeScreenView(),
//         navKey: homeNavKey,
//       ),
//       NavModel(
//         page: Center(child: Text('2',style: AppTypography().text1,),),
//         navKey: searchNavKey,
//       ),
//       NavModel(
//         page: Center(child: Text('3',style: AppTypography().text1,),),
//         navKey: notificationNavKey,
//       ),
//       NavModel(
//         page: Center(child: Text('4',style: AppTypography().text1,),),
//         navKey: profileNavKey,
//       ),
//     ];
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         if (items[selectedTab].navKey.currentState?.canPop() ?? false) {
//           items[selectedTab].navKey.currentState?.pop();
//           return Future.value(false);
//         } else {
//           return Future.value(true);
//         }
//       },
//       child: Scaffold(
//         body: IndexedStack(
//           index: selectedTab,
//           children: items
//               .map((page) => Navigator(
//             key: page.navKey,
//             onGenerateInitialRoutes: (navigator, initialRoute) {
//               return [
//                 MaterialPageRoute(builder: (context) => page.page)
//               ];
//             },
//           ))
//               .toList(),
//         ),
//         floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//         floatingActionButton: Container(
//           margin: const EdgeInsets.only(top: 10),
//           height: 64,
//           width: 64,
//           child: FloatingActionButton(
//             backgroundColor: Colors.white,
//             elevation: 0,
//             onPressed: () => debugPrint("Add Button pressed"),
//             shape: RoundedRectangleBorder(
//               side: const BorderSide(width: 3, color: Colors.green),
//               borderRadius: BorderRadius.circular(100),
//             ),
//             child: const Icon(
//               Icons.add,
//               color: Colors.green,
//             ),
//           ),
//         ),
//         bottomNavigationBar: NavBar(
//           pageIndex: selectedTab,
//           onTap: (index) {
//             if (index == selectedTab) {
//               items[index]
//                   .navKey
//                   .currentState
//                   ?.popUntil((route) => route.isFirst);
//             } else {
//               setState(() {
//                 selectedTab = index;
//               });
//             }
//           },
//         ),
//       ),
//     );
//   }
// }

