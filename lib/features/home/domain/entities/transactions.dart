class Transactions{
  final int? id;
  final String name;
  final String date;
  final String price;
  final bool isIncome;

  Transactions({this.id,required this.name, required this.date, required this.price,  this.isIncome=false});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'date': date,
      'price': price,
      'isIncome': isIncome ? 1 : 0,
    };
  }
}

