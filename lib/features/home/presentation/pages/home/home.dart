import 'package:flutter/material.dart';
import 'package:budget_app/export.dart';

import '../../provider/tansactions/local/transactions_provider.dart';
import '../../widgets/transaction_card.dart';


class HomeScreenView extends StatefulWidget {
  const HomeScreenView({super.key});

  @override
  State<HomeScreenView> createState() => _HomeScreenViewState();
}

class _HomeScreenViewState extends State<HomeScreenView> with TickerProviderStateMixin{
  late TabController _tabController;
   int index=0;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:   Consumer<TransactionProvider>(
        builder: (BuildContext context, transactionProvider, Widget? child) {
          return DefaultTabController(
            length: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 50).copyWith(bottom: 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(32.0),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [$appColors.orange, $appColors.violet],
                        begin: Alignment.topLeft,
                        end: Alignment.topRight,
                      ),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Total balance',
                            style: AppTypography().heading2
                        ),
                        const SpaceH(10),
                        Text(
                            transactionProvider.formattedTotalBalance,
                            style:  AppTypography().heading1.copyWith(fontSize: 34,)
                        ),
                        const SpaceH(20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height:32,
                                  width: 32,
                                  decoration: BoxDecoration(
                                      color: $appColors.lightGray,
                                      shape: BoxShape.circle
                                  ),
                                  child:  Icon(Icons.arrow_circle_down, color:$appColors.danger),
                                ),
                                const SpaceW(5),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Expenses',
                                      style: AppTypography().body2.copyWith(color: $appColors.white),
                                    ),
                                    const  SpaceH(5),
                                    Text(
                                      '\$ ${transactionProvider.formattedTotalExpenses}',
                                      style:AppTypography().heading3.copyWith(color: $appColors.white),
                                    ),
                                  ],
                                ),

                              ],
                            ),

                            Row(
                              children: [
                                Container(
                                  height:32,
                                  width: 32,
                                  decoration: BoxDecoration(
                                      color: $appColors.lightGray.withOpacity(0.8),
                                      shape: BoxShape.circle
                                  ),
                                  child:  Icon(Icons.arrow_circle_up_outlined, color: $appColors.success),
                                ),
                                const SpaceW(5),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Income',
                                      style: AppTypography().body2.copyWith(color: $appColors.white),
                                    ),
                                    const  SpaceH(5),
                                    Text(
                                      '\$ ${transactionProvider.formattedTotalIncome} ',
                                      style:AppTypography().heading3.copyWith(color: $appColors.white),
                                    ),
                                  ],
                                ),

                              ],
                            ),

                          ],
                        ),
                      ],
                    ),
                  ),
                  const SpaceH(20),
                  Text(
                      'Recent Transactions',
                      style: AppTypography().heading2.copyWith(color: $appColors.black)
                  ),
                  const SpaceH(20),
                  TabBar(
                    labelPadding: EdgeInsets.zero,
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.tab,
                    dividerColor: $appColors.white,
                    isScrollable: false,
                    onTap: (value){
                      setState(() {
                        index = value;
                      });
                    },
                    tabs:  [
                      Tab(
                        child: Container(
                          width: 80,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: index==0?$appColors.primary:$appColors.white,
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: index==0?$appColors.primary:$appColors.lightGray)
                          ),
                          child: Text('All',style: AppTypography().body1.copyWith(color: index==0?$appColors.white:$appColors.lightBlack),textAlign: TextAlign.center,),
                        ),
                      ),
                      Tab(
                        child:  Container(
                          width:150,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: index==1?$appColors.primary:$appColors.white,
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: index==1?$appColors.primary:$appColors.gray)
                          ),
                          child: Text('Income',style: AppTypography().body1.copyWith(color: index==1?$appColors.white:$appColors.lightBlack),textAlign: TextAlign.center,),
                        ),

                      ),
                      Tab(
                        child:  Container(
                          width: 150,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: index==2?$appColors.primary:$appColors.white,
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(color: index==2?$appColors.primary:$appColors.gray)
                          ),
                          child: Text('Expenses',style: AppTypography().body1.copyWith(color: index==2?$appColors.white:$appColors.lightBlack),textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                    ],
                  ),
                  const SpaceH(20),
                  Expanded(child: Container(
                    height: phoneHeight(context),
                    padding: const EdgeInsets.only(bottom: 50),
                    child: TabBarView(
                        controller: _tabController,
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          ListView.separated(
                            padding: const EdgeInsets.only(bottom: 50),
                            itemCount:transactionProvider.transactions.length,
                            itemBuilder: (context,index){
                              return  TransactionCards(transactions: transactionProvider.transactions.reversed.toList()[index], index: index,);
                            }, separatorBuilder: (BuildContext context, int index) {
                            return const SpaceH(15);
                          },
                          ),

                          ListView.separated(
                            padding: const EdgeInsets.only(bottom: 50),
                            itemCount:transactionProvider.transactions.where((element) => element.isIncome ==true).length,
                            itemBuilder: (context,index){
                              final incomeList =transactionProvider.transactions.where((element) => element.isIncome ==true).toList();
                              return  TransactionCards(transactions: incomeList[index],index: index,);
                            }, separatorBuilder: (BuildContext context, int index) {
                            return const SpaceH(15);
                          },
                          ),

                          ListView.separated(
                            padding: const EdgeInsets.only(bottom: 50),
                            itemCount:transactionProvider.transactions.where((element) => element.isIncome ==false).length,
                            itemBuilder: (context,index){
                              final expensesList =transactionProvider.transactions.where((element) => element.isIncome ==false).toList();
                              return  TransactionCards(transactions: expensesList.reversed.toList()[index],index: index,);
                            }, separatorBuilder: (BuildContext context, int index) {
                            return const SpaceH(15);
                          },
                          ),
                        ]),))


                ],
              ),
            ),);
        }),



    );
  }
}



