import 'dart:ui';

class AppConstant {
  AppConstant._internal();
  static const String appName = 'Home Pay';

  static const supportedLocales = [
    Locale('en'),
    Locale('fr'),
  ];


}
