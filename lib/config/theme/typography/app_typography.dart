import 'package:flutter/material.dart';

import '../color/app_color.dart';
class AppTypography {
  static const _plusJakartaSans = 'Plus Jakarta Sans';

  final heading1 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w800,
      fontSize: 24,
      color: $appColors.white);

  final heading2 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w800,
      fontSize: 18,
      color: $appColors.white);

  final heading3 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w600,
      fontSize: 14,
      color: $appColors.text);

  final body1 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w400,
      fontSize: 16,
      color: $appColors.text);

  final body2 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w400,
      fontSize: 14,
      color: $appColors.text);

  final text3 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w400,
      fontSize: 12,
      color: $appColors.text);

  final text2 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w300,
      fontSize: 12,
      color: $appColors.text);

  final text1 = TextStyle(
      fontFamily: _plusJakartaSans,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      color: $appColors.text);
}
