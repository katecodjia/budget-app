enum AppPage {
  dashboard,
  home,
}

extension AppPageExtension on AppPage {
  String get toPath {
    switch (this) {
      case AppPage.home:
        return "/home";
      case AppPage.dashboard:
        return "/dashboard";
      default:
        return "/home";
    }
  }

  String get toName {
    switch (this) {
      case AppPage.home:
        return "HOME";
      case AppPage.dashboard:
        return "DASHBOARD";
      default:
        return "HOME";
    }
  }
}