import 'package:flutter/material.dart';

class AppInkwell extends StatelessWidget {
  const AppInkwell({super.key, required this.onTap, required this.child});
  final Widget child;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      onTap: onTap,
      child: child,
    );
  }
}
