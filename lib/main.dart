import 'package:budget_app/provider_scope.dart';
import 'package:flutter/material.dart';
import 'config/theme/app_themes.dart';
import 'export.dart';

Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(Phoenix(
        child: ScreenUtilInit(
            designSize: const Size(375.0, 812.0),
            builder: (_, child) {
              return  const ProviderScope(
                child: AppTheme(),
              );
            })
    ));
  });
}
